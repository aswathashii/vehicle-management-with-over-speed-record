
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>

#define sensor_pin 4
#define pin_to_arduino 16

volatile float time_previous;
volatile int rpm;
uint8_t Kmph_value ;

const char *ssid = "L";
const char *password = "Lawliet1";

const char *host = "192.168.31.123:3000";    // Server ip address  // :8001 is port No.


void wifi_begin() {
  WiFi.mode(WIFI_OFF);
  delay(1000);
  WiFi.mode(WIFI_STA);

  WiFi.begin(ssid, password);     //Connect to your WiFi router
  Serial.println("");

  Serial.print("Connecting");
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected");
}

void ICACHE_RAM_ATTR rpm_calc()
{
  detachInterrupt(digitalPinToInterrupt(sensor_pin));
  Serial.println(rpm);
  float time_present = millis();
  Serial.println("INTERRUPT");
  Serial.println(time_present - time_previous);
  Serial.println((60 * 10e2) / (time_present - time_previous));
  rpm = (60 * 10e2) / (time_present - time_previous);
  time_previous = time_present;
  

}
void Kmph()
{

  Kmph_value = rpm * 0.079128;
  Serial.print("Kmph_value");
  Serial.println(Kmph_value);
}


void setup() {
  delay(1000);
  Serial.begin(115200);
  wifi_begin();
  pinMode(sensor_pin, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(sensor_pin), rpm_calc, CHANGE);
}

void loop() {
  wifi_upload();
  Kmph();
  if (Kmph_value > 1500)
  {
    digitalWrite(pin_to_arduino, HIGH);
    delay(1000);
    digitalWrite(pin_to_arduino, LOW);
  }
}

void wifi_upload() {
  HTTPClient http;
  String postData, value;
  value = String(rpm);

  postData = "Speed=" + value;

  http.begin("http://192.168.31.123:3000/speed/");             //page where data is to be send
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");

  if (http.POST(postData) < 0) {
    Serial.println("upload failed.... !!! ");
  }
  http.end();
}
