function viewData() {
    window.location.href='/listView';

}

function getVehicleAddress(vinNumber,ManufacturerName){    
    let keyHash  = hash(getUserPublicKey(ManufacturerName))
    let nameHash = hash("Vehicle Chain")
    let vinHash = hash(vinNumber)
    return nameHash.slice(0,6) +vinHash.slice(0,4)+keyHash.slice(0,60)

}

function addVehicleAsManufacturer(event){
    event.preventDefault();
    let privKey = document.getElementById('manPrivKey').value;
    let VinNumb = document.getElementById('VinNumbMan').value;
    let EngNumb = document.getElementById('EngNumb').value;
    let makeModel = document.getElementById('m&m').value;
    let dom = document.getElementById('dom').value;
    $.post('/addVehicle',{ key: privKey,vin:VinNumb,engine:EngNumb,model:makeModel,date:dom } ,'json');
    
}
function addVehicleAsRegister(event) {

    event.preventDefault();
    let privKey = document.getElementById('regPrivKey').value;
    let VinNumb = document.getElementById('VinNumb').value;
    let OwnName = document.getElementById('OwnName').value;
    let address = document.getElementById('address').value;
    let DOR = document.getElementById('dor').value;
    let manName = document.getElementById('manName').value;
    let PlateNo = document.getElementById('numPlate').value;
    $.post('/registerVehicle',{ key: privKey,vin:VinNumb,OwnerName:OwnName,addr:address,Dor:DOR,name:manName,plate:PlateNo } ,'json');
    
}
function speed(event) {

    event.preventDefault();
    let privKey = document.getElementById('speedPrivKey').value;
    let VinNumb = document.getElementById('VinNumb').value;
    
    let address = document.getElementById('location').value;
    let DOR = document.getElementById('date').value;
    let spd = document.getElementById('overSpeed').value;
    let PlateNo = document.getElementById('vehicleNumber').value;
    $.post('/speed',{ key: privKey,vin:VinNumb,spd:spd,addr:address,Dor:DOR,plate:PlateNo } ,'json');
}


////
/*
function listVehicles(event){
    event.preventDefault();
    console.log("listVehicles(event)-----");
    window.location.href='/vehicleList';
}
*/
