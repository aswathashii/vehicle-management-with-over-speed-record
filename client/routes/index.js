var express = require('express');
var{ Vehicle }= require('./UserClient')
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('dashboards', { title: 'Dashboards' });
});


router.get('/speed', function(req, res, next) {
  res.render('speed', { title: 'speed' });
});

router.get('/detail', function(req, res, next) {
  
  
  res.render('detail', { title: 'detail' });
});
////
router.get('/listVehicles', async (req,res)=>{
  var vehicleClient = new Vehicle();
  let stateData = await vehicleClient.getVehicleListings();
  //console.log("listings", stateData);
  let vehiclesList = [];
  stateData.data.forEach(vehicles => {
    if(!vehicles.data) return;
    let decodedVehicles = Buffer.from(vehicles.data, 'base64').toString();
    let vehicleDetails = decodedVehicles.split(',');

    //console.log("decodedVehicles------", decodedVehicles);
    vehiclesList.push({
      vinNum: vehicleDetails[1],
      engineNo: vehicleDetails[4],
      model: vehicleDetails[3],
      dom: vehicleDetails[2],
      status: (vehicleDetails.length === 5)?"Not Registered":"Registered",
      owner: vehicleDetails[6],
      address: vehicleDetails[9],
      dor: vehicleDetails[5],
      numberPlate: vehicleDetails[7],
      overspeed1:vehicleDetails[11],
      location1:vehicleDetails[10]


    });
  });

  res.render('vehicleList', { listings: vehiclesList });
});


router.get('/homePage',(req,res)=>{
  res.render('dashboards', { title: 'Dashboards' });
});

//////


router.post('/addVehicle',function(req, res){
  let key = req.body.key
  let vin = req.body.vin
  let model = req.body.model
  let dom = req.body.date
  let engineNo = req.body.engine
  console.log("Data sent to REST API");
  var client = new Vehicle();
  client.addVehicle("Manufacturer",key,vin,dom,model,engineNo)
  res.send({message: "Data successfully added"});
})
router.post('/registerVehicle',function(req, res){
  let key = req.body.key
  let vin = req.body.vin
  let OwnName = req.body.OwnerName
  let dor = req.body.Dor
  let plateNo = req.body.plate
  let address = req.body.addr
  console.log("Data sent to REST API");
  var client = new Vehicle();
  client.registerVehicle("Registrar",key,vin,dor,OwnName,plateNo,address)
  res.send({message: "Data successfully added"});
})
router.post('/speed',function(req, res){
  let key = req.body.key
  let vin = req.body.vin
  let spd = req.body.spd
  let OwnName = req.body.OwnerName
  let dor = req.body.Dor
  let plateNo = req.body.plate
  let address = req.body.addr
  console.log("Data sent to REST API");
  var client = new Vehicle();
  client.speed("vehiclespeed",key,vin,dor,OwnName,plateNo,address,spd)
  res.send({message: "Data successfully added"});
})

module.exports = router;
