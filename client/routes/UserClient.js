const {createTransaction} = require('./lib/processor')
const {hash,getVehicleAddress} = require('./lib/transaction')
const fetch = require('node-fetch');

//Approved keys for manufacturer and registrar
MANUFACTURERKEY = '8f99bb8b1dc799fd1ed9b7e370330f9378c78f7c332ac3e2233bf559ce21ea8b'
REGISTERKEY = '4206848f09f0953370fc3e4a131faeab07e239d451190294e5049cfcf05a107e'

//family name
FAMILY_NAME='Vehicle Chain'

// class for vehicle
class Vehicle{

      /* function to add newly manufactured vehicle to chain
      parameters :
      manufacturer - name of the manufacturer
      Key - key that is available to the manufacturer
      vinNumber - vehicle vinNumber
      dom - date of manufacturing
      model - model of  the car 
      engineNumber - serial no of engine
      */
      addVehicle(manufacturer,Key,vinNumber,dom,model,engineNumber){

        let address = getVehicleAddress(vinNumber)
        let action = "Add Vehicle"
        let payload = [action,manufacturer,vinNumber,dom,model,engineNumber].join(',')
        if (Key == MANUFACTURERKEY){
        	createTransaction(FAMILY_NAME,[address],[address],Key,payload)}
        else{
        	console.log('manufacturer Not Authorised')
        }

      }
      /* function to add register details manufactured vehicle to chain
      parameters :
      Registrar - Registering authority
      registrarKey - key of registrar
      vinNumber - vehicle vinNumber
      dor - date of registration
      owner - owner of  the car 
      platenumber - platenumber of car 
      address - owners address
      */

      registerVehicle(Registrar,registrarKey,vinNumber,dor,owner,plateNumber,address){
        let action = "Register Vehicle"
        let Address = getVehicleAddress(vinNumber)
        let payload = [action,vinNumber,dor,owner,plateNumber,Registrar,address].join(',')
        if (registrarKey == REGISTERKEY){
        	createTransaction(FAMILY_NAME,[Address],[Address],registrarKey,payload)
        }
        else{
        	console.log('Registrar Not Authorised')
        }
        

    }

    speed(vehiclespeed,registrarKey,vinNumber,dor,owner,plateNumber,address,spd){
      let action = "Register Over Speed"
      let Address = getVehicleAddress(vinNumber)
      let payload = [action,vinNumber,dor,owner,plateNumber,address,vehiclespeed,spd].join(',')
      if (registrarKey == REGISTERKEY){
        createTransaction(FAMILY_NAME,[Address],[Address],registrarKey,payload)
      }
      else{
        console.log('Registrar Not Authorised')
      }
      

  }


//////

/**
 * Get state from the REST API
 * @param {*} address The state address to get
 * @param {*} isQuery Is this an address space query or full address
 */
async getState (address, isQuery) {
  let stateRequest = 'http://rest-api:8008/state';
  if(address) {
    if(isQuery) {
      stateRequest += ('?address=')
    } else {
      stateRequest += ('/address/');
    }
    stateRequest += address;
  }
  let stateResponse = await fetch(stateRequest);
  let stateJSON = await stateResponse.json();
  return stateJSON;
}

async getVehicleListings() {
  let vehicleListingAddress = hash(FAMILY_NAME).substr(0, 6);
  return this.getState(vehicleListingAddress, true);
}

//////

}

module.exports = {Vehicle};
