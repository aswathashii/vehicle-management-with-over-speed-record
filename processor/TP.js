/* Transaction Processor */
const {TextDecoder} = require('text-encoding/lib/encoding')
const { TransactionHandler } = require('sawtooth-sdk/processor/handler')
const {hash , writeToStore ,getVehicleAddress } = require('./lib/transaction')
const { TransactionProcessor } = require('sawtooth-sdk/processor');



const FAMILY_NAME = "Vehicle Chain"
const NAMESPACE = hash(FAMILY_NAME).substring(0, 6);
const URL = 'tcp://validator:4004';
var decoder = new TextDecoder('utf8')

/* function to add manufactured vehicle data to chain
parameter :
context - validator context object
manufacturer - name of manufacturer
vinNumber - vehicle VIN number
dom - date of manufacturing
mode - vehicle model
engine number - engine serial number 
*/      

function addVehicle (context,manufacturer,vinNumber,dom,model,engineNumber) {
    let vehicle_Address = getVehicleAddress(vinNumber)
    let vehicle_detail =[manufacturer,vinNumber,dom,model,engineNumber]
    return writeToStore(context,vehicle_Address,vehicle_detail)
}

/* function to add  vehicle registration data to chain
parameter :
context - validator context object
Registrar - Registering authority      
vinNumber - vehicle vinNumber
dor - date of registration
owner - owner of  the car 
platenumber - platenumber of car 
*/

function registerVehicle(context,vinNumber,dor,owner,plateNumber,Register,OwnerAddress){
    console.log("registrering vehicle")
    let address = getVehicleAddress(vinNumber)
    return context.getState([address]).then(function(data){
    console.log("data",data)
    if(data[address] == null || data[address] == "" || data[address] == []){
        console.log("Invalid vin number!")
    }else{
    let stateJSON = decoder.decode(data[address])
    let newData = stateJSON + "," + [dor,owner,plateNumber,Register,OwnerAddress].join(',')
    return writeToStore(context,address,newData)
    }
    })
        

    
}


//transaction handler class

class Vehicle extends TransactionHandler{
    constructor(){
        super(FAMILY_NAME, ['1.0'], [NAMESPACE]);
    

    }
//apply function
    apply(transactionProcessRequest, context){
        let PayloadBytes = decoder.decode(transactionProcessRequest.payload)
        let Payload = PayloadBytes.toString().split(',')
        let action = Payload[0]
        if (action === "Add Vehicle"){
            return addVehicle(context,Payload[1],Payload[2],Payload[3],Payload[4],Payload[5])
        }
        else if(action === "Register Vehicle"){
            return registerVehicle(context,Payload[1],Payload[2],Payload[3],Payload[4],Payload[5],Payload[6])
        }
        else if(action ==="Register Over Speed"){
            return registerVehicle(context,Payload[1],Payload[2],Payload[3],Payload[4],Payload[5],Payload[6],Payload[7])
        }
        
    }
}

const transactionProcesssor = new TransactionProcessor(URL);
transactionProcesssor.addHandler(new Vehicle);
transactionProcesssor.start();


