/*
  LiquidCrystal Library - Hello World
  Arduino Ckt
  The circuit:
   LCD RS pin to digital pin 12
   LCD Enable pin to digital pin 11
   LCD D4 pin to digital pin 5
   LCD D5 pin to digital pin 4
   LCD D6 pin to digital pin 3
   LCD D7 pin to digital pin 2
   LCD R/W pin to ground
   LCD VSS pin to ground
   LCD VCC pin to 5V
   10K resistor:
   ends to +5V and ground
   wiper to LCD VO pin (pin 3)

*/



#include <LiquidCrystal.h>
#include <SoftwareSerial.h>

const int rs = 12, en = 11, d4 = 4, d5 = 5, d6 = 6, d7 = 7;
int rpm_signal = 2 ;

LiquidCrystal lcd(rs, en, d4, d5, d6, d7);//LCD PINS
SoftwareSerial mySerial(9, 10);


void setup()
{
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  pinMode(rpm_signal, INPUT);


  mySerial.begin(9600);   // Setting the baud rate of GSM Module
  Serial.begin(115200);    // Setting the baud rate of Serial Monitor (Arduino)
  delay(100);

  //////MAIN PROJECT//////
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("MAIN PROJECT");
  delay(2000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("SPEED DETECTION");
  lcd.setCursor(0, 1);
  lcd.print("-BLOCK CHAIN");
  delay(2000);


  //////NAMES//////
  lcd.clear();

  lcd.setCursor(0, 0);
  lcd.print("1.ashii");
  delay(1000);

  lcd.clear();
  lcd.print("2.aswath");
  delay(1000);
  lcd.clear();
  lcd.print("3.xxx");
  delay(1000);
  lcd.clear();
}

void loop() {
  Serial.print(digitalRead(rpm_signal));
  if (digitalRead(rpm_signal) == 1)
  {
    lcd_display();
    SendMessage();
  }

}

void SendMessage()
{
  mySerial.println("AT+CMGF=1");    //Sets the GSM Module in Text Mode
  delay(1000);  // Delay of 1000 milli seconds or 1 second
  mySerial.println("AT+CMGS=\"+918075546955\"\r"); // Replace x with mobile number
  mySerial.println("Car No: KL06-2828, Date 28/05/2019 time 10:45, ALERT:You have exceeded the threshold speed of 70kmph.");// The SMS text you want to send
  mySerial.println((char)26);// ASCII code of CTRL+Z

  delay(1000);
}


void RecieveMessage()
{
  mySerial.println("AT+CNMI=2,2,0,0,0"); // AT Command to receive a live SMS
  delay(1000);
}
void lcd_display()
{
  Serial.println("");
  Serial.println("INTERRUPT");
  //SendMessage();

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Message Sent");
  lcd.setCursor(0, 1);
  lcd.print("to:9037116720");
  delay(2000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("KL-06-2828");
  lcd.setCursor(0, 1);
  lcd.print("SPEED EXCEEDED");
  delay(2000);
  lcd.clear();
}
